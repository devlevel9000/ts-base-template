import { run } from "../main"

describe("Main", () => {
  test("Should be true, remove this...", () => {
    expect(1+1).toEqual(2);
  });

  test("Run function, remove this...", () => {
    expect(run()).toBeNull();
  })
});
